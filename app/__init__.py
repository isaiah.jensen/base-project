from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config.from_object('config')

db = SQLAlchemy(app)


@app.errorhandler(404)
def not_found(error):
    return "Page Not Found", 404


# add Auth Module
from app.mod_auth.controllers import auth_module
app.register_blueprint(auth_module)
db.create_all()
