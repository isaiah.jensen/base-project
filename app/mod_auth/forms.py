from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, validators


class LoginForm(FlaskForm):
    email = StringField('Email', validators=(validators.DataRequired(), validators.Email()))
    password = PasswordField("Password", validators=(validators.DataRequired(),))
