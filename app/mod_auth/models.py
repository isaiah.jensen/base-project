from app import db
from app.utils.model_utils import BaseModel
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime


class User(BaseModel):
    email = db.Column(db.String(100), nullable=False, unique=True)
    password_hash = db.Column(db.String(255), nullable=False, unique=True)
    last_pw_change = db.Column(db.DateTime)

    name = db.Column(db.String(100))

    def check_password(self, pw):
        return check_password_hash(self.password_hash, pw)

    @property
    def password(self):
        # return a blank password so that we can
        # transparently use this in forms
        return ""

    @password.setter
    def set_password(self, pw):
        self.password = generate_password_hash(pw)
        self.last_pw_change = datetime.today()
