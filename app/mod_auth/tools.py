from app import db
from werkzeug.security import generate_password_hash, check_password_hash

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    email = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)

    name = db.Column(db.String(100))

    def check_password(self, pw):
        return check_password_hash(self.password, pw)

    def set_password(self, pw):
        self.password = generate_password_hash(pw)