from flask import Blueprint, request, render_template, \
                    flash, g, session, redirect, url_for, current_app

from app.mod_auth.forms import LoginForm
from app.mod_auth.models import User


auth_module = Blueprint("auth", __name__, url_prefix="/auth", template_folder="templates", static_folder="static")

@auth_module.route("/login/", methods=["GET", "POST"])
def login():
    form = LoginForm()
    next_ = request.args.get("next", "/")

    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data

        user = User.query.filter_by(email=email).first()

        if user and user.check_password(password):
            session['user'] = user.id

            return redirect(next_)
        
        flash("Invalid Username or Password", "is-danger")

    return render_template("auth/login.html", form=form, next=next_)