import os

# Statement for enabling the development environment
DEBUG = True

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))  

# Define the database - we are working with
# SQLite for this example
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
DATABASE_CONNECT_OPTIONS = {}
SQLALCHEMY_TRACK_MODIFICATIONS = False

SESSION_COOKIE_HTTPONLY = True

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True

# Use a secure, unique and absolutely secret key for
# signing the data. 
CSRF_SESSION_KEY = "\x7fJ\xd4r\xd3/X\xa7\xca\x99\x91\xa8\xd7\xcb\x85\xc9+\xa8\xbbi\x9e\xb0\x86\x81"

# Secret key for signing cookies
SECRET_KEY = "\x08\x11\x00\x03MXDF\xb7W'\x07\xd5\xba\xd3\xd6\xf1\x06\xdd\x1a\xb5\x13\\"